import { v4 } from "uuid";

export const add_todo = (value) => {
  return {
    type: "ADD_TODO",
    id: v4(),
    value
  };
};

export const delete_todo = (id) =>{
return{
  type: "DELETE_TODO",
  id
}
}

export const clear_todo = (id) =>{
  return{
    type: "CLEAR_ALL",
    id
  }
}