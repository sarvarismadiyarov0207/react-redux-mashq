function withLog(name) {
    return function (WrapperComponent) {
      return class extends React.Component {
        state = {
          name: name
        }
  
        nameChangeHandler = (name) => {
          this.setState({
            name
          })
        }
  
        render() {
          return <WrapperComponent onNameChange={this.nameChangeHandler} name={this.state.name} />
        }
      }
    }
  
  }
  
  const App2 = (props) => {
    console.log(props)
    return <h2>{props.name}</h2>
  }
  
  const WithLog = withLog("john")(App2)