import React from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { add_todo, delete_todo } from "./actions";
// import "./App.css";

const App = ({ dispatch, globalState }) => {
  console.log(globalState);
  // console.log(props);
  // const [todos, setTodos] = React.useState([]);
  const [value, setValue] = React.useState("");
  const handlChange = (e) => {
    e.preventDefault();

    setValue(e?.target?.value);
  };

  //useDispatch(add_todo(value))
const state = useSelector(state=>state)
const dispatch = useDispatch()
  // redux action

  const onSubmit = () => {
    dispatch(add_todo(value));

    // const update = [...todos, value];
    // setTodos(update);
    setValue("");
  };

  const ondel = (id) => {
    console.log(id);
    dispatch(delete_todo(id));
  };

  return (
    <>
      <input value={value} onChange={handlChange} />
      <button onClick={onSubmit}>Add Todo</button>
      <ul>
        {/* connect usuli */}
        {/* {globalState?.map((item, id) => {
          return (
            <li onClick={ondel.bind(null, item.id)} key={id}>
              {item?.value}
              <button>Delete</button>
            </li>
          );
        })} */}

        {/* useSelect bn useDis usuli */}
         {state?.map((item, id) => {
          return (
            <li onClick={ondel.bind(null, item.id)} key={id}>
              {item?.value}
              <button>Delete</button>
            </li>
          );
        })}
      </ul>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    globalState: state,
  };
};
// connect usuli
// export default connect(mapStateToProps)(App);

// connectsiz usul
export default App
