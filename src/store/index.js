import { createStore } from "redux";
import { TODO } from "../reducers";
import throttle from "lodash/throttle";

// store o'ziga reduserlani oladi
export const store = createStore(TODO);

store.subscribe(
  throttle(() => {
    store.getState();
  }, 100)
);
